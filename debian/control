Source: rust-globset
Section: rust
Priority: optional
Build-Depends: debhelper (>= 12),
 dh-cargo (>= 25),
 cargo:native <!nocheck>,
 rustc:native <!nocheck>,
 libstd-rust-dev <!nocheck>,
 librust-aho-corasick-1+default-dev (>= 1.1.1-~~) <!nocheck>,
 librust-bstr-1+std-dev (>= 1.6.2-~~) <!nocheck>,
 librust-log-0.4+default-dev (>= 0.4.20-~~) <!nocheck>,
 librust-regex-automata-0.4+hybrid-dev <!nocheck>,
 librust-regex-automata-0.4+meta-dev <!nocheck>,
 librust-regex-automata-0.4+nfa-dev <!nocheck>,
 librust-regex-automata-0.4+perf-dev <!nocheck>,
 librust-regex-automata-0.4+std-dev <!nocheck>,
 librust-regex-automata-0.4+syntax-dev <!nocheck>,
 librust-regex-syntax-0.8+std-dev <!nocheck>
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Sylvestre Ledru <sylvestre@debian.org>
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/globset]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/globset
Homepage: https://github.com/BurntSushi/ripgrep/tree/master/crates/globset
X-Cargo-Crate: globset
Rules-Requires-Root: no

Package: librust-globset-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-aho-corasick-1+default-dev (>= 1.1.1-~~),
 librust-bstr-1+std-dev (>= 1.6.2-~~),
 librust-log-0.4+default-dev (>= 0.4.20-~~),
 librust-regex-automata-0.4+hybrid-dev,
 librust-regex-automata-0.4+meta-dev,
 librust-regex-automata-0.4+nfa-dev,
 librust-regex-automata-0.4+perf-dev,
 librust-regex-automata-0.4+std-dev,
 librust-regex-automata-0.4+syntax-dev,
 librust-regex-syntax-0.8+std-dev,
 librust-serde-1+default-dev (>= 1.0.188-~~)
Provides:
 librust-globset+default-dev (= ${binary:Version}),
 librust-globset+log-dev (= ${binary:Version}),
 librust-globset+serde-dev (= ${binary:Version}),
 librust-globset+serde1-dev (= ${binary:Version}),
 librust-globset+simd-accel-dev (= ${binary:Version}),
 librust-globset-0-dev (= ${binary:Version}),
 librust-globset-0+default-dev (= ${binary:Version}),
 librust-globset-0+log-dev (= ${binary:Version}),
 librust-globset-0+serde-dev (= ${binary:Version}),
 librust-globset-0+serde1-dev (= ${binary:Version}),
 librust-globset-0+simd-accel-dev (= ${binary:Version}),
 librust-globset-0.4-dev (= ${binary:Version}),
 librust-globset-0.4+default-dev (= ${binary:Version}),
 librust-globset-0.4+log-dev (= ${binary:Version}),
 librust-globset-0.4+serde-dev (= ${binary:Version}),
 librust-globset-0.4+serde1-dev (= ${binary:Version}),
 librust-globset-0.4+simd-accel-dev (= ${binary:Version}),
 librust-globset-0.4.15-dev (= ${binary:Version}),
 librust-globset-0.4.15+default-dev (= ${binary:Version}),
 librust-globset-0.4.15+log-dev (= ${binary:Version}),
 librust-globset-0.4.15+serde-dev (= ${binary:Version}),
 librust-globset-0.4.15+serde1-dev (= ${binary:Version}),
 librust-globset-0.4.15+simd-accel-dev (= ${binary:Version})
Description: Cross platform single glob and glob set matching - Rust source code
 Glob set matching is the process of matching one or more glob patterns against
 a single candidate path simultaneously, and returning all of the globs that
 matched.
 .
 Source code for Debianized Rust crate "globset"
